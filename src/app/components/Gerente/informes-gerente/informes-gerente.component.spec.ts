import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformesGerenteComponent } from './informes-gerente.component';

describe('InformesGerenteComponent', () => {
  let component: InformesGerenteComponent;
  let fixture: ComponentFixture<InformesGerenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformesGerenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformesGerenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
