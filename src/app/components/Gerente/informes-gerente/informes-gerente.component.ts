import { Component, OnInit } from '@angular/core';
import { GerenteServicesService } from '../../../services/gerente-services.service';
@Component({
  selector: 'app-informes-gerente',
  templateUrl: './informes-gerente.component.html',
  styleUrls: ['./informes-gerente.component.css'],
})
export class InformesGerenteComponent implements OnInit {
  informes: any[] = [];
  modal: any = {
    sake: '',
    fecha: '',
    nombre: '',
    contenido: '',
    tipo: '',
    monto: 0,
  };
  constructor(public info: GerenteServicesService) {
    this.informes = info.obtener('informes');
  }

  ngOnInit(): void {}

  abrir_Modal(i) {
    this.modal = this.informes[i];
    // console.log(this.modal);
  }
}
