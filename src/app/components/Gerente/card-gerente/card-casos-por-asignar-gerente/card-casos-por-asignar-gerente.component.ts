import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { GerenteServicesService } from '../../../../services/gerente-services.service';

@Component({
  selector: 'app-card-casos-por-asignar-gerente',
  templateUrl: './card-casos-por-asignar-gerente.component.html',
  styleUrls: ['./card-casos-por-asignar-gerente.component.css'],
})
export class CardCasosPorAsignarGerenteComponent implements OnInit {
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      },
    },
  };
  public barChartLabels;
  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  // public barChartPlugins = [pluginDataLabels];

  public barChartData;

  constructor(public Datos: GerenteServicesService) {
    let parametros = Datos.obtener('GrafPorAsignar');
    this.barChartData = parametros.EjesX;
    this.barChartLabels = parametros.EjeY;
  }

  ngOnInit(): void {}
}
