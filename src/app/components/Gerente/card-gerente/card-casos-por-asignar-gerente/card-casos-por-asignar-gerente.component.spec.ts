import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardCasosPorAsignarGerenteComponent } from './card-casos-por-asignar-gerente.component';

describe('CardCasosPorAsignarGerenteComponent', () => {
  let component: CardCasosPorAsignarGerenteComponent;
  let fixture: ComponentFixture<CardCasosPorAsignarGerenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardCasosPorAsignarGerenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardCasosPorAsignarGerenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
