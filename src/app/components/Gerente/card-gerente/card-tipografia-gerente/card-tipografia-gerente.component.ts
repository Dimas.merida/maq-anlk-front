import { Component, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
import { GerenteServicesService } from '../../../../services/gerente-services.service';

@Component({
  selector: 'app-card-tipografia-gerente',
  templateUrl: './card-tipografia-gerente.component.html',
  styleUrls: ['./card-tipografia-gerente.component.css'],
})
export class CardTipografiaGerenteComponent implements OnInit {
  // Doughnut
  doughnutChartLabels = [];
  doughnutChartData = [];
  doughnutChartType: ChartType = 'doughnut';

  constructor(public Datos: GerenteServicesService) {
    let parametros = Datos.obtener('Tipografias');
    this.doughnutChartLabels = parametros.nombres;
    this.doughnutChartData = parametros.valores;
  }

  ngOnInit(): void {}
}
