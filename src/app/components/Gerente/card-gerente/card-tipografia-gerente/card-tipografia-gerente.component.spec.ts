import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTipografiaGerenteComponent } from './card-tipografia-gerente.component';

describe('CardTipografiaGerenteComponent', () => {
  let component: CardTipografiaGerenteComponent;
  let fixture: ComponentFixture<CardTipografiaGerenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardTipografiaGerenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTipografiaGerenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
