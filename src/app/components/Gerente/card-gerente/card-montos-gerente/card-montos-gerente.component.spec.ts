import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardMontosGerenteComponent } from './card-montos-gerente.component';

describe('CardMontosGerenteComponent', () => {
  let component: CardMontosGerenteComponent;
  let fixture: ComponentFixture<CardMontosGerenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardMontosGerenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardMontosGerenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
