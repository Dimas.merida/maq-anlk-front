import { Component, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
import { GerenteServicesService } from '../../../../services/gerente-services.service';

@Component({
  selector: 'app-card-montos-gerente',
  templateUrl: './card-montos-gerente.component.html',
  styleUrls: ['./card-montos-gerente.component.css'],
})
export class CardMontosGerenteComponent implements OnInit {
  // Doughnut
  doughnutChartLabels = [];
  doughnutChartData = [];
  doughnutChartType: ChartType = 'doughnut';

  constructor(public Datos: GerenteServicesService) {
    let parametros = Datos.obtener('Montos');
    this.doughnutChartLabels = parametros.nombres;
    this.doughnutChartData = parametros.valores;
  }

  ngOnInit(): void {}
}
