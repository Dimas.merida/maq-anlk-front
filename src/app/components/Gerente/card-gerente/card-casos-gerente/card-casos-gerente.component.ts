import { Component, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
import { GerenteServicesService } from '../../../../services/gerente-services.service';

@Component({
  selector: 'app-card-casos-gerente',
  templateUrl: './card-casos-gerente.component.html',
  styleUrls: ['./card-casos-gerente.component.css'],
})
export class CardCasosGerenteComponent implements OnInit {
  // Doughnut
  doughnutChartLabels = [];
  doughnutChartData = [];
  doughnutChartType: ChartType = 'doughnut';

  constructor(public Datos: GerenteServicesService) {
    let parametros = Datos.obtener('casos');
    this.doughnutChartLabels = parametros.nombres;
    this.doughnutChartData = parametros.valores;
  }

  ngOnInit(): void {}
}
