import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardCasosGerenteComponent } from './card-casos-gerente.component';

describe('CardCasosGerenteComponent', () => {
  let component: CardCasosGerenteComponent;
  let fixture: ComponentFixture<CardCasosGerenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardCasosGerenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardCasosGerenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
