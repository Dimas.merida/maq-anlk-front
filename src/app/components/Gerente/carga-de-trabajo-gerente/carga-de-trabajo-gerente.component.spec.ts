import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CargaDeTrabajoGerenteComponent } from './carga-de-trabajo-gerente.component';

describe('CargaDeTrabajoGerenteComponent', () => {
  let component: CargaDeTrabajoGerenteComponent;
  let fixture: ComponentFixture<CargaDeTrabajoGerenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CargaDeTrabajoGerenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CargaDeTrabajoGerenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
