import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabInventarioGerenteComponent } from './tab-inventario-gerente.component';

describe('TabInventarioGerenteComponent', () => {
  let component: TabInventarioGerenteComponent;
  let fixture: ComponentFixture<TabInventarioGerenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabInventarioGerenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabInventarioGerenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
