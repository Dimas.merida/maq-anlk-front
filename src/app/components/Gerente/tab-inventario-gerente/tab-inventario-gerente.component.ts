import { Component, OnInit } from '@angular/core';
import { GerenteServicesService } from '../../../services/gerente-services.service';

@Component({
  selector: 'app-tab-inventario-gerente',
  templateUrl: './tab-inventario-gerente.component.html',
  styleUrls: ['./tab-inventario-gerente.component.css'],
})
export class TabInventarioGerenteComponent implements OnInit {
  GerenteAsignadosItems: any;
  GerentePorAsignarItems: any;
  modal: any = {};
  mensaje: string = '';
  analistas: any[] = [];
  datoBuscar: string = '';

  constructor(public Tabs: GerenteServicesService) {
    this.GerenteAsignadosItems = Tabs.obtener('Asignados');
    this.GerentePorAsignarItems = Tabs.obtener('PorAsignar');
    this.analistas = Tabs.obtener('analistas');
  }

  ngOnInit(): void {}

  abrir_modal(i) {
    this.modal = {
      ...this.GerentePorAsignarItems[i],
      tipo_caso: '',
      analista: '',
    };
    // console.log(this.modal);
  }
  async buscar() {
    if (this.datoBuscar != '') {
      let resp = await this.Tabs.buscar(this.datoBuscar);
      console.log(resp);
    }
  }
  async enviar() {
    var cheq = 0;
    for (const key in this.modal) {
      if (this.modal.hasOwnProperty(key)) {
        const element = this.modal[key];
        if (element == '' || element == 0 || element == []) {
          this.mensaje = 'No puede dejar campos vacios';
          cheq++;
        }
      }
    }
    if (cheq === 0) {
      // console.log(this.modal);
      let resp = await this.Tabs.Asignar(this.modal);
      console.log(resp);
      // refrescar formulario
      this.modal = {};
    }
  }
}
