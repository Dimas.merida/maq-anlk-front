import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideNavGerenteComponent } from './side-nav-gerente.component';

describe('SideNavGerenteComponent', () => {
  let component: SideNavGerenteComponent;
  let fixture: ComponentFixture<SideNavGerenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideNavGerenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavGerenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
