import { Component, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
import { SinorEspService } from '../../../../services/sinor-esp.service';
@Component({
  selector: 'app-card-tipografia-senor',
  templateUrl: './card-tipografia-senor.component.html',
  styleUrls: ['./card-tipografia-senor.component.css'],
})
export class CardTipografiaSenorComponent implements OnInit {
  // Doughnut
  doughnutChartLabels = [];
  doughnutChartData = [];
  doughnutChartType: ChartType = 'doughnut';

  constructor(public Datos: SinorEspService) {
    let parametros = Datos.obtener('Tipografias');
    this.doughnutChartLabels = parametros.nombres;
    this.doughnutChartData = parametros.valores;
  }

  ngOnInit(): void {}
}
