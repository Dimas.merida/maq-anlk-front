import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTipografiaSenorComponent } from './card-tipografia-senor.component';

describe('CardTipografiaSenorComponent', () => {
  let component: CardTipografiaSenorComponent;
  let fixture: ComponentFixture<CardTipografiaSenorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardTipografiaSenorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTipografiaSenorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
