import { Component, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
import { SinorEspService } from '../../../../services/sinor-esp.service';
@Component({
  selector: 'app-card-montos-senor',
  templateUrl: './card-montos-senor.component.html',
  styleUrls: ['./card-montos-senor.component.css'],
})
export class CardMontosSenorComponent implements OnInit {
  // Doughnut
  doughnutChartLabels = [];
  doughnutChartData = [];
  doughnutChartType: ChartType = 'doughnut';

  constructor(public Datos: SinorEspService) {
    let parametros = Datos.obtener('Montos');
    this.doughnutChartLabels = parametros.nombres;
    this.doughnutChartData = parametros.valores;
  }

  ngOnInit(): void {}
}
