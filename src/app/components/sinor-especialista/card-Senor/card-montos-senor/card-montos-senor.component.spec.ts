import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardMontosSenorComponent } from './card-montos-senor.component';

describe('CardMontosSenorComponent', () => {
  let component: CardMontosSenorComponent;
  let fixture: ComponentFixture<CardMontosSenorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardMontosSenorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardMontosSenorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
