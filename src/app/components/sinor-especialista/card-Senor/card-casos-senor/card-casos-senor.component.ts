import { Component, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
import { SinorEspService } from '../../../../services/sinor-esp.service';
@Component({
  selector: 'app-card-casos-senor',
  templateUrl: './card-casos-senor.component.html',
  styleUrls: ['./card-casos-senor.component.css'],
})
export class CardCasosSenorComponent implements OnInit {
  // Doughnut
  doughnutChartLabels = [];
  doughnutChartData = [];
  doughnutChartType: ChartType = 'doughnut';

  constructor(public Datos: SinorEspService) {
    let parametros = Datos.obtener('casos');
    this.doughnutChartLabels = parametros.nombres;
    this.doughnutChartData = parametros.valores;
  }

  ngOnInit(): void {}
}
