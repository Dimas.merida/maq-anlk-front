import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardCasosSenorComponent } from './card-casos-senor.component';

describe('CardCasosSenorComponent', () => {
  let component: CardCasosSenorComponent;
  let fixture: ComponentFixture<CardCasosSenorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardCasosSenorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardCasosSenorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
