import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { SinorEspService } from '../../../../services/sinor-esp.service';
@Component({
  selector: 'app-card-casos-por-asignar-senor',
  templateUrl: './card-casos-por-asignar-senor.component.html',
  styleUrls: ['./card-casos-por-asignar-senor.component.css'],
})
export class CardCasosPorAsignarSenorComponent implements OnInit {
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      },
    },
  };
  public barChartLabels;
  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  // public barChartPlugins = [pluginDataLabels];
  public barChartData;

  constructor(public Datos: SinorEspService) {
    let parametros = Datos.obtener('GrafPorAsignar');
    this.barChartData = parametros.EjesX;
    this.barChartLabels = parametros.EjeY;
  }

  ngOnInit(): void {}
}
