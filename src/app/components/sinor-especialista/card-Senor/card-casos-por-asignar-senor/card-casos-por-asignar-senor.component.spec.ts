import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardCasosPorAsignarSenorComponent } from './card-casos-por-asignar-senor.component';

describe('CardCasosPorAsignarSenorComponent', () => {
  let component: CardCasosPorAsignarSenorComponent;
  let fixture: ComponentFixture<CardCasosPorAsignarSenorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardCasosPorAsignarSenorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardCasosPorAsignarSenorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
