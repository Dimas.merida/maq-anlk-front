import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CargaDeTrabajoSinorComponent } from './carga-de-trabajo-sinor.component';

describe('CargaDeTrabajoSinorComponent', () => {
  let component: CargaDeTrabajoSinorComponent;
  let fixture: ComponentFixture<CargaDeTrabajoSinorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CargaDeTrabajoSinorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CargaDeTrabajoSinorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
