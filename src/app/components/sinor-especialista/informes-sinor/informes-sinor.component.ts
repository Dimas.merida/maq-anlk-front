import { Component, OnInit } from '@angular/core';
import { SinorEspService } from '../../../services/sinor-esp.service';

@Component({
  selector: 'app-informes-sinor',
  templateUrl: './informes-sinor.component.html',
  styleUrls: ['./informes-sinor.component.css'],
})
export class InformesSinorComponent implements OnInit {
  informes: any[] = [];
  modal: any = {
    sake: '',
    fecha: '',
    nombre: '',
    contenido: '',
    tipo: '',
    monto: 0,
  };
  constructor(public info: SinorEspService) {
    this.informes = info.obtener('informes');
  }

  ngOnInit(): void {}

  abrir_Modal(i) {
    this.modal = this.informes[i];
    // console.log(this.modal);
  }
}
