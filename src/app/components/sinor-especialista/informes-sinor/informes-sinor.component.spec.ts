import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformesSinorComponent } from './informes-sinor.component';

describe('InformesSinorComponent', () => {
  let component: InformesSinorComponent;
  let fixture: ComponentFixture<InformesSinorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformesSinorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformesSinorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
