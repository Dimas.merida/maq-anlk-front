import { Component, OnInit } from '@angular/core';
import { SinorEspService } from '../../../services/sinor-esp.service';

@Component({
  selector: 'app-tab-inventario-sinor',
  templateUrl: './tab-inventario-sinor.component.html',
  styleUrls: ['./tab-inventario-sinor.component.css'],
})
export class TabInventarioSinorComponent implements OnInit {
  SenorAsignadosItems: any[] = [];
  modal: any = {
    sake: '',
    fecha_ingreso: '',
    fecha_asignacion: '',
    tipo: '',
    analizta_asignado: '',
    status: 0,
    informe: '',
  };
  datoBuscar: string = '';

  respuesta: string = '';

  constructor(public Tabs: SinorEspService) {
    this.SenorAsignadosItems = Tabs.obtener('Asignados');
    // console.log(this.SenorAsignadosItems);
  }

  ngOnInit(): void {}

  abrirModal(i) {
    this.modal = this.SenorAsignadosItems[i];
  }

  async buscar() {
    try {
      let resp = await this.Tabs.buscar(this.datoBuscar);
      this.datoBuscar = '';
      console.log(resp);
    } catch (error) {}
  }

  async enviarInfo() {
    console.log('hola');

    let resp = this.Tabs.respuesta(this.respuesta);
    console.log(resp);
  }
}
