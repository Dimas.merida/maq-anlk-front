import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabInventarioSinorComponent } from './tab-inventario-sinor.component';

describe('TabInventarioSinorComponent', () => {
  let component: TabInventarioSinorComponent;
  let fixture: ComponentFixture<TabInventarioSinorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabInventarioSinorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabInventarioSinorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
