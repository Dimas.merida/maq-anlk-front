import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideNavSenorComponent } from './side-nav-senor.component';

describe('SideNavSenorComponent', () => {
  let component: SideNavSenorComponent;
  let fixture: ComponentFixture<SideNavSenorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideNavSenorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavSenorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
