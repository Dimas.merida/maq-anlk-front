import { Component, OnInit } from '@angular/core';
import { AlistaService } from '../../../services/alista.service';

@Component({
  selector: 'app-analisis-analista',
  templateUrl: './analisis-analista.component.html',
  styleUrls: ['./analisis-analista.component.css'],
})
export class AnalisisAnalistaComponent implements OnInit {
  acta: any = {
    fechaElaboracion: '',
    nombre: '',
    CiRifPasaporte: '',
    numero: '',
    numerode: { tipo: '', numero: '' },
    sake: '',
    ultimaFechade: { tipo: '', numero: '' },
    fechaInicio: '',
    fechaFin: '',
    FechaBloqueo: { tipo: '', numero: '' },
    totalMontoReclamado: 0,
    TotalTXReclamado: 0,
  };
  mensaje: string = '';
  constructor(public data: AlistaService) {
    // this.form = data.obtener();
  }

  ngOnInit(): void {}

  async imprimir() {
    console.log(this.acta);
  }
}
