import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalisisAnalistaComponent } from './analisis-analista.component';

describe('AnalisisAnalistaComponent', () => {
  let component: AnalisisAnalistaComponent;
  let fixture: ComponentFixture<AnalisisAnalistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalisisAnalistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalisisAnalistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
