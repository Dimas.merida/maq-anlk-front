import { Component, OnInit } from '@angular/core';
import { AlistaService } from '../../../services/alista.service';

@Component({
  selector: 'app-verificacion-analista',
  templateUrl: './verificacion-analista.component.html',
  styleUrls: ['./verificacion-analista.component.css'],
})
export class VerificacionAnalistaComponent implements OnInit {
  form: any = {
    sake: '',
    fecha: '',
    tipo: '',
    nombre: '',
    ci_rif_pasaporte: '',
    numero_cuenta: '',
    monto: '',
    itemRadius: '',
    itemCheck: [],
    competencia: '',
  };
  mensaje: string = '';
  constructor(public data: AlistaService) {
    // this.form = data.obtener();
  }

  ngOnInit(): void {}

  async itemCheck(dato) {
    var i = await this.form.itemCheck.indexOf(dato);
    // console.log(i);
    if (i == -1) {
      this.form.itemCheck.push(dato);
    } else {
      this.form.itemCheck.splice(i, 1);
    }
    // console.log(this.form.itemCheck);
  }

  async envio() {
    var cheq = 0;

    for (const key in this.form) {
      if (this.form.hasOwnProperty(key)) {
        const element = this.form[key];
        if (element == '' || element == 0 || element == []) {
          this.mensaje = 'No puede dejar campos vacios';
          cheq++;
        }
      }
    }
    if (cheq === 0) {
      let resp = await this.data.envioInformes(this.form);
      console.log(resp);
      this.form = {
        sake: '',
        fecha: '',
        tipo: '',
        nombre: '',
        ci_rif_pasaporte: '',
        numero_cuenta: '',
        monto: '',
        itemRadius: '',
        itemCheck: [],
        competencia: '',
      };
    }
  }
}
