import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificacionAnalistaComponent } from './verificacion-analista.component';

describe('VerificacionAnalistaComponent', () => {
  let component: VerificacionAnalistaComponent;
  let fixture: ComponentFixture<VerificacionAnalistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificacionAnalistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificacionAnalistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
