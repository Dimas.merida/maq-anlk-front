import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecuperacionDeFondosExtraidosComponent } from './recuperacion-de-fondos-extraidos.component';

describe('RecuperacionDeFondosExtraidosComponent', () => {
  let component: RecuperacionDeFondosExtraidosComponent;
  let fixture: ComponentFixture<RecuperacionDeFondosExtraidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecuperacionDeFondosExtraidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecuperacionDeFondosExtraidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
