import { Component, OnInit } from '@angular/core';
import { AlistaService } from '../../../services/alista.service';

@Component({
  selector: 'app-recuperacion-de-fondos-extraidos',
  templateUrl: './recuperacion-de-fondos-extraidos.component.html',
  styleUrls: ['./recuperacion-de-fondos-extraidos.component.css'],
})
export class RecuperacionDeFondosExtraidosComponent implements OnInit {
  form: any = { otrobanco: '', comentario: '' };
  mensaje: string = '';

  constructor(public data: AlistaService) {}

  ngOnInit(): void {}

  async enviar() {
    var cheq = 0;
    for (const key in this.form) {
      if (this.form.hasOwnProperty(key)) {
        const element = this.form[key];
        if (element == '' || element == 0 || element == []) {
          this.mensaje = 'No puede dejar campos vacios';
          cheq++;
        }
      }
    }
    if (cheq === 0) {
      let resp = await this.data.envioRecuperacionDeFomdos(this.form);
      console.log(resp);
      this.form = { otrobanco: '', comentario: '' };
    }
  }
}
