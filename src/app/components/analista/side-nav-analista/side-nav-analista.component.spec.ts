import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideNavAnalistaComponent } from './side-nav-analista.component';

describe('SideNavAnalistaComponent', () => {
  let component: SideNavAnalistaComponent;
  let fixture: ComponentFixture<SideNavAnalistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideNavAnalistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavAnalistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
