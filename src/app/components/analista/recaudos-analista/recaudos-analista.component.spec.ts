import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecaudosAnalistaComponent } from './recaudos-analista.component';

describe('RecaudosAnalistaComponent', () => {
  let component: RecaudosAnalistaComponent;
  let fixture: ComponentFixture<RecaudosAnalistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecaudosAnalistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecaudosAnalistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
