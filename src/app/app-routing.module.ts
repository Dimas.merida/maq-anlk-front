import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Componentes
import { HomeComponent } from './components/home/home.component';

// Gerente
import { TabInventarioGerenteComponent } from './components/Gerente/tab-inventario-gerente/tab-inventario-gerente.component';
import { CargaDeTrabajoGerenteComponent } from './components/Gerente/carga-de-trabajo-gerente/carga-de-trabajo-gerente.component';
import { InformesGerenteComponent } from './components/Gerente/informes-gerente/informes-gerente.component';

// Señor
import { TabInventarioSinorComponent } from './components/sinor-especialista/tab-inventario-sinor/tab-inventario-sinor.component';
import { CargaDeTrabajoSinorComponent } from './components/sinor-especialista/carga-de-trabajo-sinor/carga-de-trabajo-sinor.component';
import { InformesSinorComponent } from './components/sinor-especialista/informes-sinor/informes-sinor.component';

// analista
import { VerificacionAnalistaComponent } from './components/analista/verificacion-analista/verificacion-analista.component';
import { RecaudosAnalistaComponent } from './components/analista/recaudos-analista/recaudos-analista.component';
import { AnalisisAnalistaComponent } from './components/analista/analisis-analista/analisis-analista.component';
import { RecuperacionDeFondosExtraidosComponent } from './components/analista/recuperacion-de-fondos-extraidos/recuperacion-de-fondos-extraidos.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  // gerente
  {
    path: 'inventario_de_casos_Gerente',
    component: TabInventarioGerenteComponent,
  },
  {
    path: 'carga_de_trabajo_Gerente',
    component: CargaDeTrabajoGerenteComponent,
  },
  { path: 'informes_Gerente', component: InformesGerenteComponent },
  // señor
  {
    path: 'inventario_de_casos_Senor',
    component: TabInventarioSinorComponent,
  },
  {
    path: 'carga_de_trabajo_Senor',
    component: CargaDeTrabajoSinorComponent,
  },
  {
    path: 'informes_Senor',
    component: InformesSinorComponent,
  },
  // Analista
  { path: 'verificacion_analista', component: VerificacionAnalistaComponent },
  { path: 'recaudos_analista', component: RecaudosAnalistaComponent },
  { path: 'analisis_analista', component: AnalisisAnalistaComponent },
  {
    path: 'analisis_recuperacion_de_fondos_extraidos',
    component: RecuperacionDeFondosExtraidosComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
