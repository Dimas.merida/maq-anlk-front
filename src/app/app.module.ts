import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
// Librerias-Modulos
import { ChartsModule } from 'ng2-charts';

// servicios
import { GerenteServicesService } from './services/gerente-services.service';

// <------------||componentes-rutas||----------------------->

// Gerente||----->
// Components||------->

import { CargaDeTrabajoGerenteComponent } from './components/Gerente/carga-de-trabajo-gerente/carga-de-trabajo-gerente.component';
import { InformesGerenteComponent } from './components/Gerente/informes-gerente/informes-gerente.component';
import { CardCasosGerenteComponent } from './components/Gerente/card-gerente/card-casos-gerente/card-casos-gerente.component';
import { CardTipografiaGerenteComponent } from './components/Gerente/card-gerente/card-tipografia-gerente/card-tipografia-gerente.component';
import { CardMontosGerenteComponent } from './components/Gerente/card-gerente/card-montos-gerente/card-montos-gerente.component';
import { CardCasosPorAsignarGerenteComponent } from './components/Gerente/card-gerente/card-casos-por-asignar-gerente/card-casos-por-asignar-gerente.component';
import { TabInventarioGerenteComponent } from './components/Gerente/tab-inventario-gerente/tab-inventario-gerente.component';
import { SideNavGerenteComponent } from './components/Gerente/side-nav-gerente/side-nav-gerente.component';

// Rutas||----->

// señor-Analista
import { TabInventarioSinorComponent } from './components/sinor-especialista/tab-inventario-sinor/tab-inventario-sinor.component';

import { SideNavSenorComponent } from './components/sinor-especialista/side-nav-senor/side-nav-senor.component';

// analista
import { SideNavAnalistaComponent } from './components/analista/side-nav-analista/side-nav-analista.component';

// prinsipales
import { HomeComponent } from './components/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InformesSinorComponent } from './components/sinor-especialista/informes-sinor/informes-sinor.component';
import { CargaDeTrabajoSinorComponent } from './components/sinor-especialista/carga-de-trabajo-sinor/carga-de-trabajo-sinor.component';
import { CardCasosSenorComponent } from './components/sinor-especialista/card-Senor/card-casos-senor/card-casos-senor.component';
import { CardTipografiaSenorComponent } from './components/sinor-especialista/card-Senor/card-tipografia-senor/card-tipografia-senor.component';
import { CardCasosPorAsignarSenorComponent } from './components/sinor-especialista/card-Senor/card-casos-por-asignar-senor/card-casos-por-asignar-senor.component';
import { CardMontosSenorComponent } from './components/sinor-especialista/card-Senor/card-montos-senor/card-montos-senor.component';
import { VerificacionAnalistaComponent } from './components/analista/verificacion-analista/verificacion-analista.component';
import { RecaudosAnalistaComponent } from './components/analista/recaudos-analista/recaudos-analista.component';
import { AnalisisAnalistaComponent } from './components/analista/analisis-analista/analisis-analista.component';
import { RecuperacionDeFondosExtraidosComponent } from './components/analista/recuperacion-de-fondos-extraidos/recuperacion-de-fondos-extraidos.component';

@NgModule({
  declarations: [
    AppComponent,
    TabInventarioSinorComponent,
    TabInventarioGerenteComponent,
    SideNavGerenteComponent,
    SideNavSenorComponent,
    SideNavAnalistaComponent,
    HomeComponent,
    CargaDeTrabajoGerenteComponent,
    InformesGerenteComponent,
    CardCasosGerenteComponent,
    CardTipografiaGerenteComponent,
    CardMontosGerenteComponent,
    CardCasosPorAsignarGerenteComponent,
    InformesSinorComponent,
    CargaDeTrabajoSinorComponent,
    CardCasosSenorComponent,
    CardTipografiaSenorComponent,
    CardCasosPorAsignarSenorComponent,
    CardMontosSenorComponent,
    VerificacionAnalistaComponent,
    RecaudosAnalistaComponent,
    AnalisisAnalistaComponent,
    RecuperacionDeFondosExtraidosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [GerenteServicesService],
  bootstrap: [AppComponent],
})
export class AppModule {}
