import { TestBed } from '@angular/core/testing';

import { GerenteServicesService } from './gerente-services.service';

describe('GerenteServicesService', () => {
  let service: GerenteServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GerenteServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
