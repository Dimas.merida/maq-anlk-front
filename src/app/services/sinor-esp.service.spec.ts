import { TestBed } from '@angular/core/testing';

import { SinorEspService } from './sinor-esp.service';

describe('SinorEspService', () => {
  let service: SinorEspService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SinorEspService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
