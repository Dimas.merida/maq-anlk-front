import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SinorEspService {
  // Tablas
  SenorAsignadosItems: any[] = [
    {
      sake: '010-3535',
      fecha_ingreso: '25/02/2020',
      fecha_asignacion: '31/05/2020',
      tipo: 'Tipo 15',
      analizta_asignado: 'Analista 5',
      status: 190,
      informe:
        ' fmkosiosdi ifiifsdi isdfj siojfiojidfjosadakdas fmkosiosdi ifiifsdi isdfj siojfiojidfjosadakdas fmkosiosdi ifiifsdi isdfj siojfiojidfjosadakdas fmkosiosdi ifiifsdi isdfj siojfiojidfjosadakdas fmkosiosdi ifiifsdi isdfj siojfiojidfjosadakdas fmkosiosdi ifiifsdi isdfj siojfiojidfjosadakdas fmkosiosdi ifiifsdi isdfj siojfiojidfjosadakdas fmkosiosdi ifiifsdi isdfj siojfiojidfjosadakdas fmkosiosdi ifiifsdi isdfj siojfiojidfjosadakdas fmkosiosdi ifiifsdi isdfj siojfiojidfjosadakdas',
    },
    {
      sake: '',
      fecha_ingreso: '',
      fecha_asignacion: '',
      tipo: '',
      analizta_asignado: '',
      status: 100,
      informe: '',
    },
    {
      sake: '',
      fecha_ingreso: '',
      fecha_asignacion: '',
      tipo: '',
      analizta_asignado: '',
      status: 40,
      informe: '',
    },
    {
      sake: '030-3535',
      fecha_ingreso: '25/02/2020',
      fecha_asignacion: '31/05/2020',
      tipo: 'Tipo 15',
      analizta_asignado: 'Analista 5',
      status: 910,
      informe: 'fmkosiosdi ifiifsdi isdfj siojfiojidfjosadakdas',
    },
  ];

  // graficos
  CardCasos: any = {
    nombres: ['Caso 1', 'Caso 2', 'Caso 3', 'caso 4'],
    valores: [3005, 4500, 1010, 5000],
  };
  CardMontos: any = {
    nombres: ['Monto 1', 'Monto 2', 'Monto 3'],
    valores: [3005, 5000, 8000],
  };
  CardTipografias: any = {
    nombres: ['Tipo 1', 'Tipo 2'],
    valores: [3005, 5000],
  };
  CardPorAsignar: any = {
    EjeY: ['2010', '2011', '2012'],
    EjesX: [
      { data: [65, 59, 40], label: 'Caso A' },
      { data: [28, 27, 50], label: 'Caso B' },
    ],
  };

  // informes
  informes: any[] = [
    {
      sake: '100-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '500-0000',
      fecha: '03/05/2029',
      nombre: 'Luis',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '400-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '200-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '100-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '500-0000',
      fecha: '03/05/2029',
      nombre: 'Luis',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '400-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '200-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '100-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 5041240,
    },
    {
      sake: '500-0000',
      fecha: '03/05/2029',
      nombre: 'Luis',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 5030,
    },
    {
      sake: '400-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '200-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
  ];

  // formularios
  buscar(datos) {
    return 'resp hola';
  }
  respuesta(datos) {
    return 'Todo Ok';
  }

  obtener(tipoDato) {
    switch (tipoDato) {
      case 'casos':
        return this.CardCasos;
      case 'Tipografias':
        return this.CardTipografias;
      case 'Montos':
        return this.CardMontos;
      case 'GrafPorAsignar':
        return this.CardPorAsignar;
      case 'Asignados':
        return this.SenorAsignadosItems;
      case 'informes':
        return this.informes;
        break;
    }
  }
}
