import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class GerenteServicesService {
  // Datos a servir ||----->
  // analistas
  analistas: any[] = ['analista 1', 'analista 2', 'analista 3'];
  // tablas
  GerenteAsignadosItems: any[] = [
    {
      sake: '100-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      tipo: 'tipo 11',
      status: '',
    },
    {
      sake: '200-0000',
      fecha: '04/12/2009',
      nombre: 'Jose',
      tipo: 'tipo 2',
      status: '',
    },
    {
      sake: '300-0000',
      fecha: '08/11/2020',
      nombre: 'Maria',
      tipo: 'tipo 6',
      status: '',
    },
  ];
  GerentePorAsignarsItems: any[] = [
    { sake: '100-0000', fecha: '03/05/2019', nombre: 'Migel', tipo: 'tipo 1' },
    { sake: '200-0000', fecha: '04/12/2009', nombre: 'Jose', tipo: 'tipo 1' },
    { sake: '300-0000', fecha: '08/11/2020', nombre: 'Maria', tipo: 'tipo 1' },
  ];

  // informes
  informes: any[] = [
    {
      sake: '100-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '500-0000',
      fecha: '03/05/2029',
      nombre: 'Luis',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '400-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '200-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '100-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '500-0000',
      fecha: '03/05/2029',
      nombre: 'Luis',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '400-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '200-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '100-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 5041240,
    },
    {
      sake: '500-0000',
      fecha: '03/05/2029',
      nombre: 'Luis',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 5030,
    },
    {
      sake: '400-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
    {
      sake: '200-0000',
      fecha: '03/05/2019',
      nombre: 'Migel',
      contenido: 'andasm aodm oasm am dam ad msmd amd ampm p',
      tipo: 'tipo 1',
      monto: 500,
    },
  ];

  // graficos
  CardCasos: any = {
    nombres: ['Caso 1', 'Caso 2', 'Caso 3', 'caso 4'],
    valores: [3005, 4500, 1010, 5000],
  };
  CardMontos: any = {
    nombres: ['Monto 1', 'Monto 2', 'Monto 3'],
    valores: [3005, 5000, 8000],
  };
  CardTipografias: any = {
    nombres: ['Tipo 1', 'Tipo 2'],
    valores: [3005, 5000],
  };
  CardPorAsignar: any = {
    EjeY: ['2010', '2011', '2012'],
    EjesX: [
      { data: [65, 59, 40], label: 'Caso A' },
      { data: [28, 27, 50], label: 'Caso B' },
    ],
  };

  // Formularios
  Asignar(dato) {
    return dato;
  }
  buscar(dato) {
    return dato;
  }

  obtener(tipoDato) {
    switch (tipoDato) {
      case 'casos':
        return this.CardCasos;
      case 'Tipografias':
        return this.CardTipografias;
      case 'Montos':
        return this.CardMontos;
      case 'GrafPorAsignar':
        return this.CardPorAsignar;
      case 'Asignados':
        return this.GerenteAsignadosItems;
      case 'PorAsignar':
        return this.GerentePorAsignarsItems;
      case 'informes':
        return this.informes;
             case 'analistas':
        return this.analistas;
        break;
    }
  }
}
