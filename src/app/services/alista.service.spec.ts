import { TestBed } from '@angular/core/testing';

import { AlistaService } from './alista.service';

describe('AlistaService', () => {
  let service: AlistaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AlistaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
